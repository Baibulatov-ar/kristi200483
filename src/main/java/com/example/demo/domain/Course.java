package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.With;


@With
@Getter
@Setter
@AllArgsConstructor
public class Course  {
    private final Long id;
    private String author;
    private String title;
}

