package com.example.demo.controller;

import com.example.demo.dao.CourseRepository;
import com.example.demo.domain.Course;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rest/course")
@Validated
@RequiredArgsConstructor
public class RestCourseController {

    private final CourseRepository courseRepository;

    @GetMapping
    public List<Course> courseTable() {
        return courseRepository.findAll();
    }

    @GetMapping("/{id}")
    public Course courseForm(@PathVariable Long id) {
        return courseRepository.findById(id).orElseThrow();
    }

    @PostMapping("/submit")
    public List<Course> submitCourseForm(@Valid @RequestBody Course course) {
        courseRepository.save(course);
        return courseRepository.findAll();
    }

    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Long id,
                             @Valid @RequestBody Course course){
        courseRepository.findById(id).orElseThrow();
        courseRepository.save(course);
    }

    @GetMapping("/find/{prefix}")
    public List<Course> findByTitleWithPrefix(@PathVariable String prefix) {
        return courseRepository.findByTitleWithPrefix(prefix);
    }

    @DeleteMapping("/{id}")
    public List<Course> deleteCourse(@PathVariable Long id){
        courseRepository.findById(id).orElseThrow();
        courseRepository.delete(id);
        return courseRepository.findAll();
    }
}
