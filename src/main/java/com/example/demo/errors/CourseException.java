package com.example.demo.errors;

import lombok.Getter;

@Getter
public class CourseException extends RuntimeException{
    String message;
    public CourseException(String message){
        this.message=message;
    }
}

